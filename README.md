# PSONO Server - Password Manager


# Canonical source

The canonical source of PSONO Server is [hosted on GitLab.com](https://gitlab.com/psono/psono-server).


 source of PSONO Admin Client is [hosted on GitLab.com](https://gitlab.com/psono/psono-admin-client).

# pour mettre à jour
[suivre ce tuto](https://garrytrinder.github.io/2020/03/keeping-your-fork-up-to-date)

Puis :
- résoudre les conflits (changements par rapport à la version originale pour pouvoir host sur kubernetees)
- lancer le build sur OKD
